import java.util.Scanner;

/**
 * Created by amen on 8/21/17.
 */
public class StrategyStdIn implements IInputStrategy {
Scanner scanner = new Scanner(System.in);

    public StrategyStdIn() {
    }

    @Override
    public int getInt() {
    return scanner.nextInt();

    }

    @Override
    public double getDouble() {
        return scanner.nextDouble();
    }

    @Override
    public String getString() {
        return scanner.nextLine();
    }
}
