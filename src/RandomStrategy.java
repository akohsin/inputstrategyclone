import java.util.Random;

/**
 * Created by amen on 8/21/17.
 */
public class RandomStrategy implements IInputStrategy {
    Random rand = new Random(256 );

    public RandomStrategy() {
    }

    @Override
    public String getString() {
        String lancuch = "";
        for (int i = 0; i <10; i++) {
            lancuch=lancuch+(char) rand.nextInt();
        }
        return lancuch;
    }

    @Override
    public double getDouble() {
        return rand.nextDouble();
    }

    @Override
    public int getInt() {
        return rand.nextInt();
    }
}
